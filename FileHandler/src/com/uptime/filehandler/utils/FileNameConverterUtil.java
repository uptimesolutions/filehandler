/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.filehandler.utils;

import static com.uptime.filehandler.utils.JsonConverterUtil.DATE_TIME_FORMATTER;
import java.io.File;
import java.time.LocalDateTime;

/**
 *
 * @author twilcox
 */
public class FileNameConverterUtil {
    
    public static LocalDateTime fileNameConverter(String fileName) {
        StringBuilder timeStamp;
        File file;
        String[] elements;
        
        file = new File(fileName);
        fileName = file.getName().replaceFirst("[.][^.]+$", "");
        if ((elements = fileName.split("_")).length >= 7) {
            timeStamp = new StringBuilder();
            timeStamp
                    .append(elements[elements.length - 7]).append("-")
                    .append(elements[elements.length - 6].length() == 1 ? "0" + elements[elements.length - 6] : elements[elements.length - 6]).append("-")
                    .append(elements[elements.length - 5].length() == 1 ? "0" + elements[elements.length - 5] : elements[elements.length - 5]).append(" ")
                    .append(elements[elements.length - 3].length() == 1 ? "0" + elements[elements.length - 3] : elements[elements.length - 3]).append(":")
                    .append(elements[elements.length - 2].length() == 1 ? "0" + elements[elements.length - 2] : elements[elements.length - 2]).append(":")
                    .append(elements[elements.length - 1].length() == 1 ? "0" + elements[elements.length - 1] : elements[elements.length - 1]);
            try {
                return LocalDateTime.parse(timeStamp.toString(), DATE_TIME_FORMATTER);
            } catch (Exception e) {}
        }
        return LocalDateTime.now();
    }
}
