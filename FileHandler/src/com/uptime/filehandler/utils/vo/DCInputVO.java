/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.filehandler.utils.vo;

import java.time.Instant;
import java.util.Objects;

/**
 *
 * @author kpati
 */
public class DCInputVO {
    private String customer_id;
    private String site_id;
    private String site_name;
    private String device_id;
    private Instant event_timestamp;
    private String channel_num;
    private String sensor_value;

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public Instant getEvent_timestamp() {
        return event_timestamp;
    }

    public void setEvent_timestamp(Instant event_timestamp) {
        this.event_timestamp = event_timestamp;
    }

    public String getChannel_num() {
        return channel_num;
    }

    public void setChannel_num(String channel_num) {
        this.channel_num = channel_num;
    }

    public String getSensor_value() {
        return sensor_value;
    }

    public void setSensor_value(String sensor_value) {
        this.sensor_value = sensor_value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.customer_id);
        hash = 83 * hash + Objects.hashCode(this.site_id);
        hash = 83 * hash + Objects.hashCode(this.device_id);
        hash = 83 * hash + Objects.hashCode(this.event_timestamp);
        hash = 83 * hash + Objects.hashCode(this.channel_num);
        hash = 83 * hash + Objects.hashCode(this.sensor_value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DCInputVO other = (DCInputVO) obj;
        if (!Objects.equals(this.customer_id, other.customer_id)) {
            return false;
        }
        if (!Objects.equals(this.site_id, other.site_id)) {
            return false;
        }
        if (!Objects.equals(this.device_id, other.device_id)) {
            return false;
        }
        if (!Objects.equals(this.channel_num, other.channel_num)) {
            return false;
        }
        if (!Objects.equals(this.sensor_value, other.sensor_value)) {
            return false;
        }
        if (!Objects.equals(this.event_timestamp, other.event_timestamp)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DCInputVO{" + "customer_id=" + customer_id + ", site_id=" + site_id + ", device_id=" + device_id + ", event_timestamp=" + event_timestamp + ", channel_num=" + channel_num + ", sensor_value=" + sensor_value + '}';
    }
}
