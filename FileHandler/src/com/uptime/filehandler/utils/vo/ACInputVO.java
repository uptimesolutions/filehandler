/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.filehandler.utils.vo;

import java.time.Instant;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author kpati
 */
public class ACInputVO {
    private String customer_id;
    private String site_id;
    private String site_name;
    private String device_id;
    private String sample_rate;
    private int number_of_samples;
    private int channel_num;
    private Instant event_timestamp;
    private boolean is_tach;
    private boolean is_dmod;
    private List<String> waveform;
    private String echobase;
    private String echobaseTimestamp;

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getSample_rate() {
        return sample_rate;
    }

    public void setSample_rate(String sample_rate) {
        this.sample_rate = sample_rate;
    }

    public int getNumber_of_samples() {
        return number_of_samples;
    }

    public void setNumber_of_samples(int number_of_samples) {
        this.number_of_samples = number_of_samples;
    }

    public int getChannel_num() {
        return channel_num;
    }

    public void setChannel_num(int channel_num) {
        this.channel_num = channel_num;
    }

    public Instant getEvent_timestamp() {
        return event_timestamp;
    }

    public void setEvent_timestamp(Instant event_timestamp) {
        this.event_timestamp = event_timestamp;
    }

    public boolean isIs_tach() {
        return is_tach;
    }

    public void setIs_tach(boolean is_tach) {
        this.is_tach = is_tach;
    }

    public boolean isIs_dmod() {
        return is_dmod;
    }

    public void setIs_dmod(boolean is_dmod) {
        this.is_dmod = is_dmod;
    }

    public List<String> getWaveform() {
        return waveform;
    }

    public void setWaveform(List<String> waveform) {
        this.waveform = waveform;
    }

    public String getEchobase() {
        return echobase;
    }

    public void setEchobase(String echobase) {
        this.echobase = echobase;
    }

    public String getEchobaseTimestamp() {
        return echobaseTimestamp;
    }

    public void setEchobaseTimestamp(String echobaseTimestamp) {
        this.echobaseTimestamp = echobaseTimestamp;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.customer_id);
        hash = 97 * hash + Objects.hashCode(this.site_id);
        hash = 97 * hash + Objects.hashCode(this.device_id);
        hash = 97 * hash + Objects.hashCode(this.sample_rate);
        hash = 97 * hash + this.number_of_samples;
        hash = 97 * hash + this.channel_num;
        hash = 97 * hash + Objects.hashCode(this.event_timestamp);
        hash = 97 * hash + (this.is_tach ? 1 : 0);
        hash = 97 * hash + (this.is_dmod ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.waveform);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ACInputVO other = (ACInputVO) obj;
        if (this.number_of_samples != other.number_of_samples) {
            return false;
        }
        if (this.channel_num != other.channel_num) {
            return false;
        }
        if (this.is_tach != other.is_tach) {
            return false;
        }
        if (this.is_dmod != other.is_dmod) {
            return false;
        }
        if (!Objects.equals(this.customer_id, other.customer_id)) {
            return false;
        }
        if (!Objects.equals(this.site_id, other.site_id)) {
            return false;
        }
        if (!Objects.equals(this.device_id, other.device_id)) {
            return false;
        }
        if (!Objects.equals(this.sample_rate, other.sample_rate)) {
            return false;
        }
        if (!Objects.equals(this.event_timestamp, other.event_timestamp)) {
            return false;
        }
        if (!Objects.equals(this.waveform, other.waveform)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ACInputVO{" + "customer_id=" + customer_id + ", site_id=" + site_id + ", device_id=" + device_id + ", sample_rate=" + sample_rate + ", number_of_samples=" + number_of_samples + ", channel_num=" + channel_num + ", event_timestamp=" + event_timestamp + ", is_tach=" + is_tach + ", is_dmod=" + is_dmod + ", waveform=" + waveform + '}';
    }
}
