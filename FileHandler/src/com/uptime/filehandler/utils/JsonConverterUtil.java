/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.filehandler.utils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author kpati
 */
public class JsonConverterUtil {
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    public static final DateTimeFormatter DATE_TIME_OFFSET_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    
    /**
     * Private class used to convert a LocalDate json
     */
    private static class LocalDateJsonConverter implements JsonSerializer<LocalDate>, JsonDeserializer<LocalDate> {
        @Override
        public JsonElement serialize(LocalDate localDate, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(localDate.format(DateTimeFormatter.ofPattern(DATE_FORMAT)));
        }

        @Override
        public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext jsonSerializationContext) throws JsonParseException {
            return LocalDate.parse(json.getAsString());
        }
    }
    
    /**
     * Private class used to convert a LocalDateTime json
     */
    private static class LocalDateTimeJsonConverter implements JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {
        @Override
        public JsonElement serialize(LocalDateTime localDateTime, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(localDateTime.format(DATE_TIME_FORMATTER));
        }

        @Override
        public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext jsonSerializationContext) throws JsonParseException {
            return LocalDateTime.parse(json.getAsString(), DATE_TIME_FORMATTER);
        }
    }
    
    /**
     * Private class used to convert an Instant json
     */
    private static class InstantJsonConverter implements JsonSerializer<Instant>, JsonDeserializer<Instant> {
        private final ZoneId ZONE_ID;
        
        /**
         * Param Constructor
         * @param zoneId, ZoneId Object 
         */
        public InstantJsonConverter(ZoneId zoneId) {
            ZONE_ID = zoneId;
        }

        @Override
        public JsonElement serialize(Instant instant, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(DATE_TIME_OFFSET_FORMATTER.withZone(ZONE_ID).format(instant));
        }

        @Override
        public Instant deserialize(JsonElement json, Type type, JsonDeserializationContext jsonSerializationContext) throws JsonParseException {
            return LocalDateTime.parse(json.getAsString(), DATE_TIME_OFFSET_FORMATTER).atZone(ZONE_ID).toInstant();
        }
    }
    
    /**
     * Convert an object to a String Object json
     * @param object, Object
     * @param zoneId, ZoneId Object
     * @return String Object
     * @throws java.lang.Exception
     */
    public static String toJSON(Object object, ZoneId zoneId) throws Exception {
        GsonBuilder builder;
        
        if(object == null || zoneId == null) {
            throw new NullPointerException("One or more given params are null");
        }
        
        builder = new GsonBuilder();
        builder.setDateFormat(DATE_FORMAT);
        builder.registerTypeAdapter(LocalDate.class, new LocalDateJsonConverter());
        builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter());
        builder.registerTypeAdapter(Instant.class, new InstantJsonConverter(zoneId));
        
        return builder.disableHtmlEscaping().create().toJson(object);
    }
}
