/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.filehandler.utils.thread;

import com.uptime.filehandler.FileHandler;
import static com.uptime.filehandler.FileHandler.fileNameCount;
import static com.uptime.filehandler.FileHandler.fileNames;
import static com.uptime.filehandler.FileHandler.mutex;
import com.uptime.filehandler.utils.JsonConverterUtil;
import static com.uptime.filehandler.utils.JsonConverterUtil.DATE_TIME_FORMATTER;
import com.uptime.filehandler.utils.vo.ACInputVO;
import com.uptime.filehandler.utils.vo.DCInputVO;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class Task implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Task.class.getName());
    private final String FILE_NAME;
    private String RMS;
    private final float RMS_DIFFERENCE_PCT = 0.5f;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    /**
     * Constructor
     * @param fileName, String Object 
     */
    public Task(String fileName) {
        this.FILE_NAME = fileName;
        RMS = null;
    }

    /**
     * Task to be performed
     */
    @Override
    public void run() {
        File file = null;
        int responseCode = 0;
        boolean delete = false;
        boolean move = false;
        StringBuilder sb;
        List<String> jsons = null;
        
        if (FILE_NAME != null && !FILE_NAME.isEmpty()) {
            //Logger.getLogger(Task.class.getName()).log(Level.INFO, "*********Start Task For:{0}", FILE_NAME);
            
            try{
                // parse the data file
                file = new File(FILE_NAME);
                if(file.getName().contains("DC_")){
                    jsons = getDcInputJson(file);
                    for(String s : jsons){
                        LOGGER.info("POST JSON:"+s);
                    }
                }
                else
                    jsons = getAcInputJson(file);
            }
            catch(Exception e){
                // send email notification
                FileHandler.reportException(e);
                LOGGER.error(e.getMessage(), e);
            }
                
            // HTTP POST the file to UpcastCM
            if(jsons != null && !jsons.isEmpty()){
                for(String s : jsons){
                    try{
                        responseCode = doPost(s);
                        if(responseCode == 200){
                            delete = true;
                            move = false;
                        }
                        else{
                            // send email notification
                            FileHandler.reportException(new Exception("HTTP POST to SensorData/inbound failed ("+responseCode+")"));
                            
                            // for non HTTP 200 move file to ERROR_DIR
                            LOGGER.warn("*********Response code:"+responseCode+" for File name:"+ file.getName());
                            LOGGER.warn("Moving file to ERROR_DIR:"+ file.getName());
                            delete = false;
                            move = true;
                        }
                    }
                    catch(Exception e){
                        // if there is an exception during HTTP, set everything to false so the file will be readded to the queue to be reprocessed
                        delete = false;
                        move = false;
                    }
                }
            }
            else{
                LOGGER.info("Deleting invalid file: " + file.getName());
                delete = true;
                move = false;
            }
                
                
            if (delete || move) {
                try {
                    // If delete equals true, delete the file so it won't be processed again.
                    if(delete){
                        Files.delete(file.toPath());
                        LOGGER.info("*********Deleted file:" + file.getAbsolutePath());
                    }
                    // If move equals true, move the file to the ErrorFiles
                    if(move){
                        Files.move(Paths.get(file.getAbsolutePath()), Paths.get(FileHandler.ERROR_DIR+File.separator+file.getName()), ATOMIC_MOVE);
                        LOGGER.info("*********Moved file:" + file.getAbsolutePath());
                    }
                    fileNameCount.decrementAndGet();

                } catch (IOException e) {
                    // send email notification
                    FileHandler.reportException(e);
                    
                    LOGGER.error(e.getMessage(), e);
                }
            } 
            // readd the file to the queue to be reprocessed
            else {
                try {
                    mutex.acquire();
                    fileNames.add(FILE_NAME);
                } catch (InterruptedException e) {
                    // send email notification
                    FileHandler.reportException(e);
                    LOGGER.error(e.getMessage(), e);
                } finally {
                    mutex.release();
                }
            }
        }
    }

    /**
     * Do a HTTP POST the given JSON
     * @param json, String Object
     * @return int
     */
    private int doPost(String json) throws Exception{
        URL urlObj;
        HttpURLConnection con;
        byte[] input;

        input = json.getBytes(StandardCharsets.UTF_8);

        urlObj = new URL(FileHandler.properties.getProperty("httpPostURL"));
        con = (HttpURLConnection) urlObj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty( "charset", "utf-8");
        con.setRequestProperty("BASESTATION", FileHandler.HOSTNAME);
        con.setRequestProperty("HWID", FileHandler.properties.getProperty("macAddress"));
        con.setRequestProperty("HWIDTOKEN", FileHandler.properties.getProperty("token"));
        con.setConnectTimeout(5000);
        con.setReadTimeout(20000); // 20 seconds because of slow cell networks
        con.setDoInput(true);
        con.setDoOutput(true);

        try (DataOutputStream writer = new DataOutputStream( con.getOutputStream())) {
            writer.write(input);
            writer.flush();
            writer.close();
        }
        
        int response = con.getResponseCode();
        con.disconnect();
        return response;
    }

    /**
     * Create and return the DC Input json based on the given file
     * @param file, File Object
     * @return String Object
     */
    private synchronized List<String> getDcInputJson(File file) throws Exception{
        NumberFormat numberFormat;
        String value;
        String[] elements;
        DCInputVO dcInputVO;
        ArrayList<String> dcJsons = new ArrayList<>();
        String customer = FileHandler.properties.getProperty("customerAccount");
        String siteId = FileHandler.properties.getProperty("siteId");
        String siteName = FileHandler.properties.getProperty("siteName");
        //String customer = "90000";
        //String siteId = "OP";
        //String siteName = "OP";
        
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(file));
            // process file if it is not empty
            if ((value = reader.readLine()) != null) {
                elements = value.split("\\s+");
                if(elements != null && elements.length > 0){
                    switch(elements[1].substring(0, 2).toUpperCase()) {
                        //serial # decoding: TS1 = prefix '00', Mist = prefix 'BB', Storm = prefix 'BA', BF = StormXRTD, and BE = StormXT

                        case "BB": // MIST
                            numberFormat = NumberFormat.getInstance();
                            numberFormat.setMaximumFractionDigits(2);

                            //For a Mist:
                            //Bit 0 = Temperature
                            //Bit 1 = Battery
                            value = Integer.toBinaryString(Integer.parseInt(elements[2], 16));
                            if (value.trim().length() > 0) {
                                if (value.trim().substring(0, 1).equals("1")) {
                                    dcInputVO = new DCInputVO();
                                    dcInputVO.setCustomer_id(customer);
                                    dcInputVO.setSite_id(siteId);
                                    dcInputVO.setSite_name(siteName);
                                    dcInputVO.setEvent_timestamp((LocalDateTime.parse(getDCDate(elements[0]), DATE_TIME_FORMATTER).atZone(TimeZone.getDefault().toZoneId()).toInstant()));
                                    dcInputVO.setDevice_id(elements[1].toUpperCase());
                                    dcInputVO.setChannel_num("0");
                                    dcInputVO.setSensor_value(numberFormat.format(Float.parseFloat(elements[3])));
                                    dcJsons.add(JsonConverterUtil.toJSON(dcInputVO, TimeZone.getDefault().toZoneId()));
                                }

                                if (value.trim().length() > 1 && value.trim().substring(1).equals("1")) {
                                    dcInputVO = new DCInputVO();
                                    dcInputVO.setCustomer_id(customer);
                                    dcInputVO.setSite_id(siteId);
                                    dcInputVO.setSite_name(siteName);
                                    dcInputVO.setEvent_timestamp((LocalDateTime.parse(getDCDate(elements[0]), DATE_TIME_FORMATTER).atZone(TimeZone.getDefault().toZoneId()).toInstant()));
                                    dcInputVO.setDevice_id(elements[1].toUpperCase());
                                    dcInputVO.setChannel_num("1");
                                    dcInputVO.setSensor_value(numberFormat.format(Float.parseFloat(elements[4])));
                                    dcJsons.add(JsonConverterUtil.toJSON(dcInputVO, TimeZone.getDefault().toZoneId()));
                                }
                            }  
                            break;
                        case "00": // TS1
                            numberFormat = NumberFormat.getInstance();
                            numberFormat.setMaximumFractionDigits(5);
                            value = Integer.toBinaryString(Integer.parseInt(elements[2], 16));
                            LOGGER.info("MASK:"+value);
                            int numSensors = value.trim().length();
                            //if (value.trim().length() == 7) {
                                for(int i=0; i<numSensors; i++){
                                    if (Float.parseFloat(elements[3+i]) != -1.0) {
                                        dcInputVO = new DCInputVO();
                                        dcInputVO.setCustomer_id(customer);
                                        dcInputVO.setSite_id(siteId);
                                        dcInputVO.setSite_name(siteName);
                                        dcInputVO.setEvent_timestamp((LocalDateTime.parse(getDCDate(elements[0]), DATE_TIME_FORMATTER).atZone(TimeZone.getDefault().toZoneId()).toInstant()));
                                        dcInputVO.setDevice_id(elements[1].toUpperCase());
                                        dcInputVO.setChannel_num(Integer.toString(i));
                                        LOGGER.info("SENSOR:"+dcInputVO.getChannel_num());
                                        //System.out.println("SENSOR:"+dcInputVO.getChannel_num());
                                        dcInputVO.setSensor_value(numberFormat.format(Float.parseFloat(elements[3+i])));
                                        //System.out.println("VALUE:"+dcInputVO.getSensor_value());
                                        LOGGER.info("VALUE:"+dcInputVO.getSensor_value());
                                        dcJsons.add(JsonConverterUtil.toJSON(dcInputVO, TimeZone.getDefault().toZoneId()));
                                    }
                                }
                            //}
                            break;
                        case "BA": // STORM
                            break;
                        case "BF": // STORMXRTD
                            break;
                        case "BE": // STORMXT
                            break;
                    }
                }
            }
        }
        catch(Exception e){
            throw e;
        }
        finally{
            if(reader != null)
                reader.close();
        }
        
        return dcJsons;
    }

    /**
     * Create and return the AC Input json based on the given file
     * @param file, File Object
     * @return String Object
     */
    private synchronized List<String> getAcInputJson(File file) throws Exception{
        List<String> waveforms;
        ACInputVO acInput;
        NumberFormat numberFormat;
        List<Float> values;
        String[] element;
        String line;
        ArrayList<String> jsons = new ArrayList<>();
        
        BufferedReader reader = new BufferedReader(new FileReader(file));
        waveforms = new ArrayList();
        numberFormat = NumberFormat.getInstance();
        acInput = new ACInputVO();
        values = new ArrayList();

        acInput.setChannel_num(-1);
        acInput.setCustomer_id(FileHandler.properties.getProperty("customerAccount"));
        acInput.setSite_id(FileHandler.properties.getProperty("siteId"));
        acInput.setSite_name(FileHandler.properties.getProperty("siteName"));
        ZonedDateTime echoDate = ZonedDateTime.ofInstant(Instant.ofEpochMilli(file.lastModified()), ZoneId.systemDefault());
        acInput.setEchobaseTimestamp(formatter.format(echoDate));
        //acInput.setCustomer_id("55180"); // only for local testing - comment out for production use
        //acInput.setSite_id("OP"); // only for local testing - comment out for production use
        //acInput.setSite_name("OP");
        while ((line = reader.readLine()) != null) {
            //System.out.println("LINE length:"+line.length());
            if (line.startsWith(";")) {
                if (line.startsWith(";PodID")) {
                    // Mist starts with BBE or BBC, Storm starts with BA0, Brick starts with 001
                    acInput.setDevice_id(line.replaceAll(";PodID", "").trim().toUpperCase());
                    continue;
                }
                if (line.startsWith(";Samples")) {
                    acInput.setNumber_of_samples(Integer.parseInt(line.replaceAll(";Samples", "").trim()));
                    continue;
                }
                if (line.contains("Date Year")) {
                    acInput.setEvent_timestamp((LocalDateTime.parse(getACDate(line), DATE_TIME_FORMATTER).atZone(TimeZone.getDefault().toZoneId()).toInstant()));
                    continue;
                }
                if (line.startsWith(";FSampleRate")) {
                    numberFormat.setMinimumFractionDigits(2);
                    acInput.setSample_rate(numberFormat.format(Float.parseFloat(line.replaceAll(";FSampleRate", "").trim())).replace(",", ""));
                    continue;
                }
                if (line.startsWith(";nStart_channel")) {
                    int chNum = -1;
                    // check if nStart_channel == T
                    String s = line.replaceAll(";nStart_channel", "").trim();
                    if(s.compareToIgnoreCase("T") != 0)
                        chNum = Integer.parseInt(s);

                    if(acInput.getDevice_id().startsWith("BBE") || acInput.getDevice_id().startsWith("BBC")){
                        // Mist: for raw data files ;nStart_channel 1=ultra and 2=vibe
                        if(chNum == 1) // ultra
                            acInput.setChannel_num(0);
                        if(chNum == 2) // vibe
                            acInput.setChannel_num(1);
                    }
                    else if(acInput.getDevice_id().startsWith("001")){
                        // TS1 brick: 0-2, 4-7, 3=tach which is not supported yet
                        if(chNum > 0 && chNum <= 3)
                            acInput.setChannel_num(chNum-1);
                        else if(chNum == -1)
                            acInput.setChannel_num(3); // tach
                        else if(chNum >= 4)
                            acInput.setChannel_num(chNum);
                    }
                    else if(acInput.getDevice_id().startsWith("BA")){
                        // storm
                    }
                    
                    continue;
                }
                if (line.startsWith(";channelIds")) {
                    element = line.split("\\s+");
                    if (element.length > 3) {
                        acInput.setIs_dmod(element.length > 3 && element[3].equals("1"));
                    }
                    if (element.length > 2) {
                        acInput.setIs_tach(element[2].equals("1"));
                    }
                    continue;
                }
                if(line.startsWith(";echobase")){
                    acInput.setEchobase(line.replaceAll(";echobase", "").trim().toUpperCase());
                    // update echobase status
                    FileHandler.ECHOBASES.put(acInput.getEchobase(), acInput.getEchobaseTimestamp());
                    continue;
                }
                if (line.contains("RMS")) {
                    RMS = line.replaceAll(";RMS", "").trim();
                    continue;
                }
            }
            else if(line.startsWith(" ")){
                // ignore line that start with a space
            }
            else {
                Float f;
                try{
                    f =Float.parseFloat(line);
                    values.add(f);
                }
                catch(Exception e){
                    reader.close();
                    // the data is invalid so return null
                    LOGGER.error(e.getMessage(), e);
                    return null;
                }
            }
        }
        reader.close();

        // check that the file was valid by ensuring all the properties are set
        if(acInput.getDevice_id() == null || acInput.getDevice_id().length() < 1){
            throw new Exception("Input file is not valid.");
        }
        if(acInput.getChannel_num() == -1){
            throw new Exception("Input file is not valid.");
        }
        if(acInput.getEvent_timestamp() == null){
            throw new Exception("Input file is not valid.");
        }
        if(acInput.getNumber_of_samples() == 0){
            throw new Exception("Input file is not valid.");
        }

        if (acInput.getNumber_of_samples() == values.size() && rmsCheck(values)) {
            numberFormat.setMinimumFractionDigits(11);
            values.forEach(sample -> waveforms.add(numberFormat.format(sample)));
            acInput.setWaveform(waveforms);

            String s = JsonConverterUtil.toJSON(acInput, TimeZone.getDefault().toZoneId());
            jsons.add(s);
            return jsons;
        }
        else{
            if (acInput.getNumber_of_samples() != values.size())
                LOGGER.error("Samples ("+acInput.getNumber_of_samples()+") does not match actual ("+values.size()+")");
            if(!rmsCheck(values))
                LOGGER.error("RMS check failed.");
        }
        return null;
    }

    /**
     * Return the AC Date from the given String
     * @param date, String Object
     * @return String Object
     * @throws Exception
     */
    private String getACDate(String date) throws Exception {
        Matcher m;
        String d = "", s;
        int i = 0;
        
        m = Pattern.compile("\\((.*?)\\)").matcher(date);
        while (m.find()) {
            
            //added prefix 0 for single digit year/month/day/time components
            s = m.group(1).length() < 2 ? "0" + m.group(1) : m.group(1);
            if (i <= 2) {
                if (i == 2) {
                    d += s + " ";
                } else {
                    d += s + "-";
                }
            } else if (i > 2 && i < 6) {
                if (i == 5) {
                    d += s;
                } else {
                    d += s + ":";
                }
            } else if (i == 6) {
                s = s.length() < 3 ? "0" + s : s;
                d += "." + s;
            }
            
            i++;
        }
        
        if (!d.contains(".")) {
            d = d + ".000";
        }
        
        return d;

    }

    /**
     * Return the DC Date from the given String
     * @param date, String Object
     * @return String Object
     * @throws Exception
     */
    private String getDCDate(String date) throws Exception {
        String d = "", item;
        String[] dateTimeStringArr;
        
        dateTimeStringArr = date.split("__");
        for (String s : dateTimeStringArr[0].split("_")) {
            //added prefix 0 for single digit year/month/day components
            s = s.length() >= 2 ? s : "0" + s;
            d += s + "-";
        }
        
        d = d.subSequence(0, (d.length() - 1)).toString() + " ";
        dateTimeStringArr = dateTimeStringArr[1].split("_");
        for (int i = 0; i < dateTimeStringArr.length; i++) {
            item = dateTimeStringArr[i];
            
            //added prefix 0 for single digit time components
            item = item.length() < 2 ? "0" + item : item;
            if (i < 3) {
                if (i == 2) {
                    d += item;
                } else {
                    d += item + ":";
                }
            } else if (i == 3) {
                //added prefix 0 for single digit time components
                item = item.length() < 3 ? "0" + item : item;
                d += "." + item;
            }
        }
        
        if (!d.contains(".")) {
            d = d + ".000";
        }
        
        return d;
    }

    /**
     * Check the rms for the given AC file
     * @param values, Array Object of floats
     * @return boolean, true if match, else false
     */
    private boolean rmsCheck(List<Float> values) {
        float rms = 0.0f, rootMeanSquare;
        String[] elements;
        BigDecimal bigDecimal;
        
        try {
            if (values.isEmpty()) {
                if (RMS == null) {
                    return true;
                } else {
                    return Float.parseFloat(RMS) == rms;
                }
            }

            for (float sample : values) {
                rms += sample * sample;
            }
            rms /= values.size();
            rootMeanSquare = (float) Math.sqrt(rms);
            System.out.println("RMS:"+rootMeanSquare);

            RMS = RMS.contains(".") ? RMS.replaceAll("0*$","").replaceAll("\\.$","") : RMS;
            if (RMS.contains(".")) {
                if ((elements = RMS.split("\\.")).length != 2) {
                    return false;
                } else {
                    bigDecimal = new BigDecimal(rootMeanSquare);
                    bigDecimal = bigDecimal.setScale(elements[1].length(), BigDecimal.ROUND_HALF_UP);
                    rootMeanSquare = bigDecimal.floatValue();
                }
            }
            
            float f = Math.abs(Float.parseFloat(RMS) - rootMeanSquare);
            f = (f / Float.parseFloat(RMS)) * 100;
            if(f < RMS_DIFFERENCE_PCT)
                return true;
            else
                return false;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }
    
    public static void main(String[] args){
        File f = new File("D:\\temp\\filehandler\\DC_test.txt");
        Task t = new Task("D:\\temp\\filehandler\\DC_test.txt");
        //t.run();
        try{
            //List<String> l = t.getAcInputJson(f);
            List<String> l = t.getDcInputJson(f);
            for(String s : l){
                System.out.println(s);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
