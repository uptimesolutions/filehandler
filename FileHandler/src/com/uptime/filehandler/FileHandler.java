/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.filehandler;

import com.datastax.driver.core.utils.UUIDs;
import com.google.gson.JsonParser;
import static com.uptime.filehandler.utils.FileNameConverterUtil.fileNameConverter;
import com.uptime.filehandler.utils.handler.RejectionHandler;
import com.uptime.filehandler.utils.thread.Task;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class FileHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileHandler.class.getName());
    public static final ArrayDeque<String> fileNames = new ArrayDeque();
    public static final AtomicLong fileNameCount = new AtomicLong(0);
    public static final Semaphore mutex = new Semaphore(1);
    public static ThreadPoolExecutor poolExecutor;
    private static String OS;
    private static File CONFIG_FILE;
    private static final int BLOCK_QUEUE_SIZE = 5;
    public static File ERROR_DIR = null;
    public static Properties properties;
    public static String HOSTNAME;
    public static ConcurrentHashMap<String,String> ECHOBASES = new ConcurrentHashMap();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Check for arguments
        if(args.length < 1){
            System.out.println("Usage: FileHandler <config-file-path>");
            System.exit(0);
        }
        
        try{
            HOSTNAME = InetAddress.getLocalHost().getHostName();
        }
        catch(UnknownHostException e){
            System.out.println("Cannot get hostname of this system.");
            System.exit(1);
        }
        
        // Check for config file
        CONFIG_FILE = new File(args[0]);
        //CONFIG_FILE = new File("D:\\projects\\filehandler\\FileHandler\\Config.xml");
        
        if(!CONFIG_FILE.isFile()){
            System.out.println("Config file is not a file.");
            System.exit(1);
        }else if(!CONFIG_FILE.canRead()){
            System.out.println("Cannot read config file.");
            System.exit(1);
        }
        
        // Set Properties from Config file
        readPropertiesFromConfigXml();
        
        // Set current os
        OS = System.getProperty("os.name").substring(0, 3).toLowerCase();
        
        // Create error dir
        if (properties.getProperty("errorDir"+ "_" + OS) != null){
            ERROR_DIR = new File(properties.getProperty("errorDir"+ "_" + OS));
            // create the directory if it doesn't exist
            if(!ERROR_DIR.exists()){
                if(!ERROR_DIR.mkdir()){
                    LOGGER.error("Cannot create ERROR_DIR:"+ERROR_DIR.getAbsolutePath());
                    System.exit(1);
                }
            }
            // check if it is a directory
            if(!ERROR_DIR.isDirectory()){
                LOGGER.error("ERROR_DIR is not a directory:"+ERROR_DIR.getAbsolutePath());
                System.exit(1);
            }
            LOGGER.info("*****errorDir: " + ERROR_DIR.getAbsolutePath());
        }
        else{
            LOGGER.error("errorDir is not configured in config file.");
            System.exit(1);
        }

        /*
        Set poolExecutor:
            The corePoolSize parameter is the number of core threads that will be instantiated and kept in the pool. 
            When a new task comes in, if all core threads are busy and the internal queue is full, the pool is 
            allowed to grow up to maximumPoolSize.
            The keepAliveTime parameter is the interval of time for which the excessive threads 
            (instantiated in excess of the corePoolSize) are allowed to exist in the idle state. By default, 
            the ThreadPoolExecutor only considers non-core threads for removal. In order to apply the same removal 
            policy to core threads, we can use the allowCoreThreadTimeOut(true) method.
            once all maximumPoolSize threads are running and if new task is arriving then that new task will be rejected and will be handled by RejectionHandler.
        */
        poolExecutor = new ThreadPoolExecutor(1, Integer.parseInt(properties.getProperty("maxPoolSize")), 5, TimeUnit.SECONDS, new ArrayBlockingQueue<>(BLOCK_QUEUE_SIZE), Executors.defaultThreadFactory(), new RejectionHandler());

        // Start Token Check Task
        checkToken();
        
        // Start Polling Directory Task
        pollDirectoryAtRegularInterval();
    }

    /**
     * After each iteration of processing file, upload the airbase and echobases status
     */
    private static void uploadBasestationStatus(){
        if(!ECHOBASES.isEmpty()){
            StringBuilder json = new StringBuilder();
            json.append("{\"customer_id\":\"").append(properties.getProperty("customerAccount")).append("\",");
            json.append("\"site_id\":\"").append(properties.getProperty("siteId")).append("\",");
            json.append("\"site_name\":\"").append(properties.getProperty("siteName")).append("\",");
            json.append("\"airbase\":\"").append(HOSTNAME).append("\",");
            json.append("\"echobases\":[");
            ECHOBASES.forEach((k, v) -> { json.append("{\"echobase\":\""+ k + "\",\"last_update\":\"" + v +"\"},"); });
            json.deleteCharAt(json.length()-1);
            json.append("]}");
            LOGGER.info("BASESTATION STATUS:"+json.toString());

            try{
                int response = doPost(json.toString());
                if(response != 200){
                    throw new Exception("Error uploading basestation status. Response code "+response);
                }
            }
            catch(Exception e){
                LOGGER.error(e.getMessage(), e);
                reportException(e);
            }
        }
    }
    /**
     * Main Application Task
     * This is a continues task
     */
    private static void pollDirectoryAtRegularInterval() {
        new Timer().schedule(new TimerTask() {
            
            @Override
            public void run() {
                // only poll for files if all existing files have been processed.
                if (fileNameCount.get() < 1 && fileNames.isEmpty()) {
                    pollingDirectory();
                    try{
                        // sleep 3 seconds so files that are still being written to can complete
                        Thread.sleep(3000);
                    }
                    catch(InterruptedException e){
                        LOGGER.error(e.getMessage(), e);
                    }
                    // reset the hashmap of echobases and airbases
                    ECHOBASES.clear();
                    // process the data files
                    processFiles();
                    // upload the airbase and echobase status
                    uploadBasestationStatus();
                } else if (!fileNames.isEmpty()) {
                    // process the data files
                    processFiles();
                    // upload the airbase and echobase status
                    uploadBasestationStatus();
                }
            }
            
        }, 0, (Integer.parseInt(properties.getProperty("pollingInterval")) * 1000 * 60));

    }

    /**
     * check the current Task and refresh if needed
     */
    private static void checkToken() {
        long unixTimestampInMill, delay;
        Instant tokenInstant, now;
        //Properties properties;
        boolean check;
        
        try {
            LOGGER.info("*****Checking Token*****");
            
            // Check the token and get new if needed
            do {
                if (properties.getProperty("token") != null) {
                    unixTimestampInMill = UUIDs.unixTimestamp(UUID.fromString(properties.getProperty("token")));
                    tokenInstant = Instant.ofEpochMilli(unixTimestampInMill);
                    now = Instant.now(Clock.systemUTC());
                    if (tokenInstant.isAfter(now.minus(22, ChronoUnit.HOURS))) {
                        LOGGER.info("*****Token still valid.*****");
                        check = true;
                    } else if (tokenInstant.isAfter(now.minus(24, ChronoUnit.HOURS))) {
                        check = getNewToken("get");
                    } else {
                        check = getNewToken("post");
                    }
                } else {
                    check = getNewToken("post");
                }
                
                if (!check) {
                    Thread.sleep(1000 * 60); // sleep 60 seconds before checking the token again
                }
            } while (properties.getProperty("token") == null && !check);
            
            // Update the Config file
            try{
                FileOutputStream out = new FileOutputStream(CONFIG_FILE);
                properties.storeToXML(out, "Properties for FileHandler");
                out.close();
            } catch (Exception e) {
                throw e;
            }
            
            // Setting delay
            // (1000 mill) * (60 sec) * (60 min) * (22 hour) = 79200000
            unixTimestampInMill = UUIDs.unixTimestamp(UUID.fromString(properties.getProperty("token")));
            now = Instant.now(Clock.systemUTC());
            
            if ((delay = (unixTimestampInMill + 79200000L) - (now.getEpochSecond() * (long)1000)) < 0) {
                delay = 0L;
            }
            
            LOGGER.info("*****The delay is: " +(delay / 3600000f)+" hours");
            
            // Set Timer for next token update
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    checkToken();
                }
            }, delay);
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.exit(1);
        }
    }

    /**
     * Read the needed properties from the config file
     */
    private static void readPropertiesFromConfigXml() {
        try {
            FileInputStream fileInput = new FileInputStream(CONFIG_FILE);
            properties = new Properties();
            properties.loadFromXML(fileInput);
            fileInput.close();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.exit(1);
        }
    }

    /**
     * Get the file names from the directories
     */
    private static void pollingDirectory() {
        List<String> directories, files, allFiles;
        String dirToPollFiles;
        
        fileNames.clear();
        dirToPollFiles = properties.getProperty("dirToPoll" + "_" + OS);
        LOGGER.info("*****dirToPoll: " + dirToPollFiles);
        
        /**
         * For testing purposes we will be reading from the TS1Backup folder which places all files in the same directory
         
        try (Stream<Path> filesStream = Files.list(Paths.get(dirToPollFiles))) {
            files = filesStream.filter(file -> !Files.isDirectory(file)).filter(extension -> extension.toString().endsWith(".txt")).map(Path::toAbsolutePath).map(Path::toString).collect(Collectors.toList());
            if (files != null) {
                allFiles = new ArrayList();
                if(files.size() > 0) {
                    allFiles.addAll(files);
                }
                
                if (!allFiles.isEmpty()) {
                    fileNames.addAll(allFiles.stream().filter(fileName -> fileName.split("_").length >= 7).sorted((s1, s2) -> fileNameConverter(s2).compareTo(fileNameConverter(s1))).collect(Collectors.toList()));
                    fileNameCount.set((long)fileNames.size());
                }
                LOGGER.info("Files to process:"+fileNameCount);
            }
        }
        catch(IOException e){
            LOGGER.error(e.getMessage(), e);
            files = null;
        }
        */
        /**
         * Under normal operation the TS1 writes file to individual directories
         */
        // The TS1 creates a directory for each Mist. First get the list of Mist directories.
        try (Stream<Path> filesStream = Files.list(Paths.get(dirToPollFiles))) {
            directories = filesStream.filter(file -> Files.isDirectory(file)).map(Path::toAbsolutePath).map(Path::toString).collect(Collectors.toList());
        } catch(IOException e){
            LOGGER.error(e.getMessage(), e);
            directories = null;
        }
        
        // for each Mist directory get the data files to process
        if (directories != null) {
            allFiles = new ArrayList();
            for (String dir : directories) {
                try (Stream<Path> filesStream = Files.list(Paths.get(dir))) {
                    files = filesStream.filter(file -> !Files.isDirectory(file)).filter(extension -> extension.toString().endsWith(".txt")).map(Path::toAbsolutePath).map(Path::toString).collect(Collectors.toList());
                } catch(Exception e){
                    LOGGER.error(e.getMessage(), e);
                    files = null;
                }

                if (files != null) {
                    if(files.size() > 0) {
                        allFiles.addAll(files);
                    }
                }
            }
            
            if (!allFiles.isEmpty()) {
                fileNames.addAll(allFiles.stream().filter(fileName -> fileName.split("_").length >= 7).sorted((s1, s2) -> fileNameConverter(s2).compareTo(fileNameConverter(s1))).collect(Collectors.toList()));
                fileNameCount.set((long)fileNames.size());
            }
        }
    }

    /**
     * Set Tasks for each file and execute
     */
    private static void processFiles() {
        long unixTimestampInMill;
        Instant tokenInstant, now;
        int count;
        
        try {
            
            // Token check
            if (properties.getProperty("token") != null) {
                unixTimestampInMill = UUIDs.unixTimestamp(UUID.fromString(properties.getProperty("token")));
                tokenInstant = Instant.ofEpochMilli(unixTimestampInMill);
                now = Instant.now(Clock.systemUTC());
                if (tokenInstant.isAfter(now.minus(24, ChronoUnit.HOURS))) {
                    
                    // Process files if Token and Connection are both good
                    if (connectionCheck()) {
                        count = 0;
                        while (fileNameCount.get() > 0) {
                            try {
                                mutex.acquire();
                                while (fileNames.peek() != null && poolExecutor.getQueue().size() < 5) {
                                    poolExecutor.execute(new Task(fileNames.pop()));
                                    ++count;
                                }
                            } finally {
                                mutex.release();
                            } 
                            
                            if (count == 1000) {
                                if (connectionCheck()) {
                                    count = 0;
                                } else {
                                    break;
                                }
                            }
                        }
                    } 
                }
                
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
    
    /**
     * Check the connection of the POST application
     * @return boolean, true if able to connect, else false
     */
    private static boolean connectionCheck() {
        HttpURLConnection connection;
        URL url;
        
        try {
            // Connection Check
            url = new URL(properties.getProperty("httpConnectionCheckURL"));
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            int code = connection.getResponseCode();
            if(code == 200)
                return true;
            else
                LOGGER.warn(url+" unavailable");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            
        }
        return false;
    }
    
    /**
     * Do an http request to get a new token
     * @param requestType
     * @return 
     */
    private static boolean getNewToken(String requestType) {
        StringBuilder sb;
        HttpURLConnection con;
        byte[] input;
        String line;
        URL url;
        
        try {
            if (requestType == null || (!requestType.toLowerCase().equals("post") && !requestType.toLowerCase().equals("get"))) {
                throw new Exception("Invalid request type.");
            } else {
                
                // Do HTTP GET request to the BaseStationAuth application
                if (requestType.toLowerCase().equals("get")) {
                    sb = new StringBuilder();
                    sb
                            .append(properties.getProperty("httpAuthURL"))
                            .append("?customer=").append(URLEncoder.encode(properties.getProperty("customerAccount"), "UTF-8"))
                            .append("&siteId=").append(properties.getProperty("siteId"));
                    
                    LOGGER.info("*****HTTP GET URL:" +sb.toString());
                    url = new URL(sb.toString());
                    con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("HWID", properties.getProperty("macAddress"));
                    con.setRequestProperty("HWIDTOKEN", properties.getProperty("token"));
                    con.setConnectTimeout(5000);
                    con.setReadTimeout(5000);
                    con.connect();
                } 
                
                // Do HTTP POST request to the BaseStationAuth application
                else {
                    sb = new StringBuilder();
                    sb
                            .append("{\"customerAccount\":\"").append(properties.getProperty("customerAccount")).append("\",")
                            .append("\"siteId\":\"").append(properties.getProperty("siteId")).append("\",")
                            .append("\"passcode\":\"").append(properties.getProperty("passcode")).append("\"}");
                    
                    input = sb.toString().getBytes(StandardCharsets.UTF_8);
                    
                    url = new URL(properties.getProperty("httpAuthURL"));
                    LOGGER.info("*****HTTP POST URL:" +url.toString());
                    
                    con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty( "charset", "utf-8");
                    con.setRequestProperty("HWID", properties.getProperty("macAddress"));
                    con.setConnectTimeout(5000);
                    con.setReadTimeout(5000);
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.connect();

                    try (DataOutputStream writer = new DataOutputStream( con.getOutputStream())) {
                        writer.write(input);
                    }
                }
                
                // If 200 response received from BaseStationAuth, process Json for new token 
                if (con.getResponseCode() == 200) {
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                        sb = new StringBuilder();
                        while ((line = br.readLine()) != null) {
                            sb.append(line.trim());
                        }
                    }

                    try {
                        LOGGER.info("*****Obtained new Token.*****");
                        properties.setProperty("token", JsonParser.parseString(sb.toString()).getAsJsonObject().get("issuedToken").getAsString());
                        return true;
                    } catch (Exception e) {
                        throw e;
                    }
                } 
                
                else {
                    LOGGER.warn("Base Station Auth response code:" + con.getResponseCode());
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getErrorStream(), StandardCharsets.UTF_8))) {
                        sb = new StringBuilder();
                        while ((line = br.readLine()) != null) {
                            sb.append(line.trim());
                        }
                    }
                    LOGGER.warn("Base Station Auth response body:" + sb.toString());
                }
                
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            //System.exit(1);
        }
        
        LOGGER.warn("*****Failed to update token.*****");
        return false;
    }
    
    public static void reportException(Exception ex){
        URL urlObj;
        HttpURLConnection con;
        // convert exceptin to a String
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String stackTrace = sw.toString();
        pw.close();

        try{
            StringBuilder sb = new StringBuilder();
            sb.append("{\"customer_acct\":\"").append(FileHandler.properties.getProperty("customerAccount")).append("\",");
            sb.append("\"site_id\":\"").append(FileHandler.properties.getProperty("siteName")).append("\",");
            sb.append("\"hostname\":\"").append(System.getenv("COMPUTERNAME")).append("\",");
            sb.append("\"exception_time\":\"").append(Instant.now().toString()).append("\",");
            sb.append("\"exception\":\"").append(stackTrace).append("\"}");

            urlObj = new URL(FileHandler.properties.getProperty("exceptionPostURL"));
            con = (HttpURLConnection) urlObj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty( "charset", "utf-8");
            con.setRequestProperty("HWID", FileHandler.properties.getProperty("macAddress"));
            con.setRequestProperty("HWIDTOKEN", FileHandler.properties.getProperty("token"));
            con.setConnectTimeout(5000);
            con.setReadTimeout(20000); // 20 seconds because of slow cell networks
            con.setDoInput(true);
            con.setDoOutput(true);

            try (DataOutputStream writer = new DataOutputStream( con.getOutputStream())) {
                writer.write(sb.toString().getBytes());
                writer.flush();
                writer.close();
            }

            int response = con.getResponseCode();
            con.disconnect();
            LOGGER.warn("POST to Exception API: "+sb.toString());
            
            if(response != 200)
                LOGGER.error("SEVERE: POST to Exception API response "+response);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
    
    /**
     * Do a HTTP POST the given JSON
     * @param json, String Object
     * @return int
     */
    public static int doPost(String json) throws Exception{
        URL urlObj;
        HttpURLConnection con;
        byte[] input;

        input = json.getBytes(StandardCharsets.UTF_8);

        urlObj = new URL(properties.getProperty("httpPostURL"));
        con = (HttpURLConnection) urlObj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty( "charset", "utf-8");
        con.setRequestProperty("BASESTATION", FileHandler.HOSTNAME);
        con.setRequestProperty("HWID", properties.getProperty("macAddress"));
        con.setRequestProperty("HWIDTOKEN", properties.getProperty("token"));
        con.setConnectTimeout(5000);
        con.setReadTimeout(20000); // 20 seconds because of slow cell networks
        con.setDoInput(true);
        con.setDoOutput(true);

        try (DataOutputStream writer = new DataOutputStream( con.getOutputStream())) {
            writer.write(input);
            writer.flush();
            writer.close();
        }
        
        int response = con.getResponseCode();
        con.disconnect();
        return response;
    }
}
